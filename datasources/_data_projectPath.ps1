function _load_projectPath {
    # Auto-require path to ensure functionality
    if (-not (Get-DataSource -Name path)) {
        Add-DataSource -Name path
    }

    return [promptData]@{
        name = "projectPath"
        async = 0
        refresh = 1
        projectRoot = 'projectPath-projectRoot'
        outputs = {
            $root = Get-ParentItem -Path $prompt['path-current'] -Filter ".project"
            if ($root) {
                $out = @{
                    projectRoot = Split-Path $root.fullname
                }
                $out
            }
        }
    }
}

