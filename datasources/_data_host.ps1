function _load_host {
    if (
        $PSVersiontable.PSEdition -eq 'Desktop' -or
        $PSVersiontable.Platform -eq 'Win32NT'
    ) {
        return [promptData]@{
            name    = "host"
            async   = 0
            refresh = 0
            outputs = {
                return @{
                    USER      = $env:USERNAME
                    DOMAIN    = $env:USERDOMAIN
                    COMPUTER  = $env:COMPUTERNAME
                    PSEdition = $PSVersionTable.PSEdition
                    PSVersion = $PSVersionTable.PSVersion
                }
            }
        }
    } else {
        return [promptData]@{
            name    = "host"
            async   = 0
            refresh = 0
            outputs = {
                @{
                    USER      = $env:USER
                    COMPUTER  = hostname
                    PSEdition = $PSVersionTable.Edition
                    PSVersion = $PSVersionTable.PSVersion
                }
            }
        }
    }
}
