function _load_clock {
    return [promptData]@{
        name = "clock"
        async = 1
        refresh = "Prompt"
        outputs = {
            param($data)
            $time = Get-Date
            if (-not ($time.Second % $data.AsyncInterval)) {
                return @{
                    Date = $time
                }
            } else {
                return $false
            }
        }
        AsyncMethod = "Timer"
        # Timer actually runs at 1s interval, use AsyncInterval as #
        # of seconds to actually perform a refresh at.
        AsyncInterval = 5
    }
}
