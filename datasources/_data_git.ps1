function _load_git {
    return [promptData]@{
        name        = "git"
        async       = 1
        refresh     = -1
        AsyncMethod = "Job"
        projectRoot = 'git-projectRoot'
        outputs     = {
            # Empty Form
            $emptyOut = @{
                gitDir         = $null
                projectRoot    = $null
                aheadBy        = $null
                behindBy       = $null
                upstreamGone   = $null
                upstream       = $null
                upstreamHost   = $null
                index          = $null
                working        = $null
                untracked      = $null
                branch         = $null
                stash          = $null
                commit         = $null
                branchState    = $null
                branchRelative = $null
            }
            # Ensure actually in a filesystem before looking for a root
            $pathInfo = Microsoft.PowerShell.Management\Get-Location
            if (!$pathInfo -or ($pathInfo.Provider.Name -ne 'FileSystem')) {
                return $emptyOut
            }
            else {
                # Find git directory through rev-parse
                $root = git rev-parse --git-dir 2>$null
                if ($root) {
                    $gitRoot = Resolve-Path $root
                }
            }
            if ($gitRoot) {
                $gitIndex = @{
                    added    = [System.Collections.ArrayList]@()
                    modified = [System.Collections.ArrayList]@()
                    deleted  = [System.Collections.ArrayList]@()
                    unmerged = [System.Collections.ArrayList]@()
                }
                $gitWorking = @{
                    added    = [System.Collections.ArrayList]@()
                    modified = [System.Collections.ArrayList]@()
                    deleted  = [System.Collections.ArrayList]@()
                    unmerged = [System.Collections.ArrayList]@()
                }
                $gitUntracked = [System.Collections.ArrayList]@()
                $bare = git rev-parse --is-bare-repository 2>$null
                if ($bare -eq 'true') {
                    $gitCommit = "BARE_REPO"
                }
                else {
                    $gitCommit = git rev-parse HEAD 2>$null | ForEach-Object {
                        if ($_ -match "HEAD") {
                            "0000000"
                        }
                        else {
                            $_.substring(0, 7)
                        }
                    }
                }

                # Status
                $status = git -c core.quotepath=false -c color.status=false status -z --branch 2>$null
                $status = $status -split [char]0
                switch -regex ($status) {
                    '^(?<untracked>(?<index>[^#])(?<working>.)) (?<path1>.*?)(?: -> (?<path2>.*))?$' {
                        $gitBranchState = 'Dirty'
                        switch ($matches['index']) {
                            'A' {
                                $gitIndex.added.Add($matches['path1']) | Out-Null
                                break
                            }
                            'C' {
                                $gitIndex.modified.Add($matches['path1']) | Out-Null
                                break
                            }
                            'D' {
                                $gitIndex.deleted.Add($matches['path1']) | Out-Null
                                break
                            }
                            'M' {
                                $gitIndex.modified.Add($matches['path1']) | Out-Null
                                break
                            }
                            'R' {
                                $gitIndex.modified.Add($matches['path1']) | Out-Null
                                break
                            }
                            'U' {
                                $gitIndex.unmerged.Add($matches['path1']) | Out-Null
                                break
                            }
                        }
                        switch ($matches['working']) {
                            'A' {
                                $gitworking.added.Add($matches['path1']) | Out-Null
                                break
                            }
                            'D' {
                                $gitWorking.deleted.Add($matches['path1']) | Out-Null
                                break
                            }
                            'M' {
                                $gitWorking.modified.Add($matches['path1']) | Out-Null
                                break
                            }
                            'U' {
                                $gitWorking.unmerged.Add($matches['path1']) | Out-Null
                                break
                            }
                            '?' {
                                $gitWorking.added.Add($matches['path1']) | Out-Null
                            }
                        }
                        if ($matches['untracked'] -eq '??') {
                            $gitUntracked.Add($matches['path1']) | Out-Null
                        }
                        continue
                    }
                    '^## (?<branch>\S+?)(?:\.\.\.(?<upstream>\S+))?(?: \[(?:ahead (?<ahead>\d+))?(?:, )?(?:behind (?<behind>\d+))?(?<gone>gone)?\])?$' {
                        $gitBranch = $matches['branch']
                        $gitUpstream = $matches['upstream']
                        $gitAhead = $matches['ahead']
                        $gitBehind = $matches['behind']
                        $gitUpstreamGone = $matches['gone']
                        continue
                    }
                    '^## (Initial commit|No commits yet) on (?<branch>\S+)' {
                        $gitBranch = $matches['branch']
                        $gitBranchState = 'Initial'
                        continue
                    }
                    '^## HEAD (no branch)' {
                        $gitBranchState = 'Detached'
                        continue
                    }
                }
                # Branch
                if (!$gitBranch) {
                    ## Use git branch command
                    $gitBranch = git branch 2>$null | Where-Object {
                        $_ -match "^\*"
                    } | ForEach-Object {
                        switch -Regex ($_) {
                            "detached" {
                                $b = $_ -replace ".*detached at ([0-9a-z]{7})\)", '$1'
                                # $r = git describe --contains --all HEAD
                                # $gitBranchState = "Detached"
                                # if ($r) {
                                #     $gitBranchRelative = $r
                                # }
                                return $b
                            }
                            default {
                                return $_ -replace '^\* '
                            }
                        }
                    }
                    ## Try parsing .git/HEAD
                    if (!$gitBranch) {
                        if ($gitCommit -eq '0000000') {
                            $head = Join-Path $gitRoot "HEAD"
                            $gitBranch = (get-content $head) -replace '.*refs/heads/'
                            $gitBranchState = 'Initial'
                        }
                    }
                }
                #Git Stash
                $Stash = git stash list -z 2>$null
                if ($Stash) {
                    $gitStash = $stash -split [char]0
                } else {
                    $gitStash = $null
                }
                if (!$gitBranchState) {
                    $gitBranchState = 'Clean'
                }
                $projectRoot = Split-Path -parent $gitRoot
                if ($gitUpstream) {
                    $url = git remote get-url ($gitUpstream -replace '/.*')
                    $upRegex = '(https?://|git@)(?<upstream>\w+)\..*'
                    $gitUpstreamHost = $url -replace $upRegex,'${upstream}'
                }
                $out = @{
                    aheadBy        = $gitAhead
                    behindBy       = $gitBehind
                    branch         = $gitBranch
                    branchRelative = $gitBranchRelative
                    branchState    = $gitBranchState
                    commit         = $gitCommit
                    index          = $gitIndex
                    projectRoot    = $projectRoot
                    stash          = $gitStash
                    untracked      = $gitUntracked
                    upstream       = $gitUpstream
                    upstreamGone   = $gitUpstreamGone
                    upstreamHost   = $gitUpstreamHost
                    working        = $gitWorking
                    gitDir         = $gitRoot.path
                }
                return $out
            }
            else {
                return $emptyOut
            }
        }
    }
}
