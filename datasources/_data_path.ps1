
function _load_path {
    [CmdletBinding()]
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidGlobalVars', "")]
    param()
    return [promptData]@{
        name = "path"
        async = 0
        refresh = 1
        outputs = {
            if ($global:_getLoc -and $global:_getLoc.Ast.ToString() -eq '{ Get-Location -Stack }') {
                $stack = &$_getLoc
            }
            $out = @{
                current  = $PWD
            }
            if ($stack) {
                $out.stack = $stack
            }
            $out
        }
    }
}
