function _load_history {
    return [promptData]@{
        name = "history"
        async = 0
        refresh = 1
        outputs = {
            $id = $prompt['history-Id']
            $history = Get-History -id $id
            $duration = New-Timespan $history.StartExecutionTime $history.EndExecutionTime
            @{
                StartExec    = $history.StartExecutionTime
                EndExec      = $history.EndExecutionTime
                Duration     = $duration
                Id           = $history.id
                Command      = $history.commandline
            }
        }
    }
}
