function _load_terraform {
    # Auto-require path to ensure functionality
    if (-not (Get-DataSource -Name path)) {
        Add-DataSource -Name path
    }

    return [promptData]@{
        name = "terraform"
        async = 0
        refresh = 1
        projectRoot = 'terraform-projectRoot'
        outputs = {
            $root = Get-ParentItem -Path $prompt['path-current'] -Filter ".terraform"
            if ($root) {
                $out = @{
                    projectRoot = Split-Path $root.fullname
                }
            } else {
                $out = @{
                    projectRoot = $null
                }
            }
            $out
        }
    }
}

