function _load_project {
    # Auto-require path to ensure functionality
    if (-not (Get-DataSource -Name path)) {
        Add-DataSource -Name path
    }

    return [promptData]@{
        name = "project"
        async = 0
        refresh = 1
        outputs = {
            # Identify Projects
            $root = $prompt.GetEnumerator() | Where-Object {
                $_.key -match 'projectRoot'
            } | Where-Object {
                $prompt['path-current'] -match ($_.value -replace '\\','\\')
            } | Sort-Object value -Descending | Select-Object -first 1

            if ($root) {
                $path = $prompt['path-current'] -replace ($root.value -replace '\\','\\'),''
                @{
                    root = $root.value
                    projectPath = $path -replace '^(\\|/)',''
                    rootType = $root.key -replace '-.*',''
                }
            }
        }
    }
}
