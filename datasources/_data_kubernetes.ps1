function _load_kubernetes {

    if (-not (Get-Command kubectl)) {
        Write-Warning "kubectl binary not found.  This datasource may fail to work"
    }
    TRY {
        $kubeConfigPath = Resolve-Path "~/.kube/config" -ErrorAction Stop
    }
    CATCH {
        Write-Error "Kubeconfig file not present.  Cannot load datasource"
        break
    }
    return [promptData]@{
        name = "kubernetes"
        async = 1
        refresh = 1
        outputs = {
            $kubeConfigPath = Resolve-Path "~/.kube/config"
            $kubeconfig = Get-Content $kubeConfigPath | ConvertFrom-Yaml
            $context = $kubeconfig.'current-context'
            $namespace = (
                $kubeconfig.contexts | Where-Object {
                    $_.name -eq $context
                }
            ).context.namespace
            @{
                Cluster = $context
                Namespace = $namespace
            }
        }
        AsyncMethod = "FileWatch"
        AsyncFile = $kubeConfigPath
    }
}
