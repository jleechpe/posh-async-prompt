BeforeAll {
    . $PSScriptRoot/../functions/ConvertTo-ColoredString.ps1
}

Describe "Validate-Color" {
    It "Takes a valid hex string" {
        Validate-Color "#ffffff" | Should -BeTrue
        Validate-Color "#FFFFFF" | Should -BeTrue
        Validate-Color "#0011AA" | Should -BeTrue
    }
    It "Rejects invalid Hex" {
        Validate-Color "#GG0000" | Should -BeFalse
        Validate-Color "#AABBC" | Should -BeFalse
        Validate-Color "#AABBCCDD" | Should -BeFalse
        Validate-Color "AABBCC" | Should -BeFalse
    }
    It "Takes valid System Colors" {
        Validate-Color "Red" | Should -BeTrue
        Validate-Color "Gray" | Should -BeTrue
    }
    It "Rejects invalid System Colors" {
        Validate-Color "Grey" | Should -BeFalse
        Validate-Color "Pink" | Should -BeFalse
    }
}
