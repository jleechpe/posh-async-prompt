Add-Datasource path
Add-Datasource history
Add-Datasource host
Add-Datasource git
Add-Datasource kubernetes
Add-Datasource clock
#Add-Datasource Project

$backSep = [char]::ConvertFromUtf32(0xE0B3)

Set-PSReadLineOption -ExtraPromptLineCount 1

Set-Prompt -index 0 -side left -line 1 -fg "#26C6DA" -bg "#546E7A" -Content {
    $sep = ([char]::ConvertFromUtf32(0xE0B1))
    $splat = @{
        ForegroundColor = "#26C6DA"
        BackgroundColor = "#546E7A"
        TextOnly = $true
    }
    @(
        ConvertTo-ColoredString -text (" {0} " -f $prompt['host-PSEdition']) @splat
        ConvertTo-ColoredString -text $sep @splat
    ) -join ''
}

Set-Prompt -index 1 -side left -line 1 -fg "#26C6DA" -bg "#546E7A" -Content {
    " {0} " -f $prompt['path-current']
}

Set-Prompt -index 2 -side left -line 1 -Content {
    $sep = ([char]::ConvertFromUtf32(0xE0B1))
    if ($prompt['git-gitDir']) {
        @(
            ConvertTo-ColoredString -text $sep -textonly -ForegroundColor "#26C6DA" -BackgroundColor "#546E7A"
            ConvertTo-ColoredString -text "[ GIT ]" -textonly -ForeGroundColor "#D4E157" -BackgroundColor "#546E7A"
        ) -join ''
    }
}

Set-Prompt -index 3 -side left -line 1 -fg "#546E7A" -Content {
    "{0}" -f [char]::ConvertFromUtf32(0xE0B0)
}

Set-Prompt -index 0 -side right -line 1 -Content {
    $splat = @{
        ForeGroundColor = "#D4E157"
        BackgroundColor =  "#546E7A"
        TextOnly = $true
    }
    @(
        ConvertTo-ColoredString -textonly -foregroundColor "#546E7A" -text "$([char]::ConvertFromUtf32(0xE0B2))"
        ConvertTo-ColoredString @splat -text (" {0}@{1}" -f $prompt['host-USER'],$prompt['host-COMPUTER'])
        ConvertTo-ColoredString @splat -text $backsep
    ) -join ''
}

Set-Prompt -index 1 -side right -line 1 -fg "#D4E157" -bg "#546E7A" -Content {
    $time = if ($prompt['clock-Date']) {
        $prompt['clock-Date']
    } else { Get-Date }
    " {0:hh:mm:ss} {1}" -f [datetime]$time,[char]::ConvertFromUtf32(0xF017)
}

Set-Prompt -index 0 -side left -line 2 -Content {
    $char = [char]::ConvertFromUtf32(0x276F)
    if ($prompt['history-lastExit'] -eq $true) {
        ConvertTo-ColoredString -textonly -ForeGroundColor '#D4E157' -Text $char
    } else {
        ConvertTo-ColoredString -textonly -ForegroundColor '#FF5252' -text $char
    }
}
