Set-PromptSeparator -side left -char ' '
Add-Datasource path
Add-Datasource history
Add-Datasource host
#Add-Datasource git
Add-Datasource kubernetes
#Add-Datasource Project

Set-PSReadLineOption -ExtraPromptLineCount 1

# Path
Set-Prompt -index 0 -side left -line 1 -fg "#ffffff" -Content {
    " {0} " -f $prompt['path-current']
}
# Git
Set-Prompt -index 1 -side left -line 1 -fg "#C2C206" -Content {
    if ($prompt['git-gitDir']) {
        Get-GitPrompt
    }
}

# Root

# Exit
Set-Prompt -index 2 -side left -line 1 -fg "#C94A16" -Content {
    if ($prompt['history-lastExit'] -eq $false) {
        " x{0} " -f $prompt['history-error']
    }
}

Set-Prompt -index 0 -side left -line 2 -fg "#007ACC" -Content {
    [char]::ConvertFromUtf32(0xE602)
}
