Set-PromptSeparator -side left -char ([char]::ConvertFromUtf32(0xE0B1))
Set-PromptSeparator -side right -char ([char]::ConvertFromUtf32(0xE0B3))
Add-Datasource path
Add-Datasource history
Add-Datasource project
Add-Datasource git
Add-Datasource kubernetes
Set-Prompt -index 0  -line 1 -side left -fg "#00FF33" -Content {
    if ($prompt['project-root']) {
        "{0} " -f $prompt['project-root']
    } else {
        $prompt['path-current']
    }
}
Set-Prompt -index 1 -line 1 -side left -FG Blue -Content {
    " {0} " -f $prompt['git-commit']
}
Set-Prompt -index 2 -line 1 -side left -fg yellow -content {
    if ($prompt['project-root']) {
        " {0} " -f $prompt['project-projectPath']
    }
}
Set-Prompt -index 3 -line 1 -side left -fg darkred -NoSeparator -content {
    " {0} " -f $prompt['kubernetes-Cluster']
}
Set-Prompt -line 1 -side right -fg yellow -content {
    " {0} " -f $prompt['history-Id']
}
Set-Prompt -line 1 -side right -fg darkred -content {
    " {0:mm}:{0:ss} " -f $prompt['history-Duration']
}
Set-Prompt -line 2 -side left -NoSeparator -content  {
    " PS >"
}
Set-PSReadLineOption -ExtraPromptLineCount 1
