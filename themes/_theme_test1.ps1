Set-PromptSeparator -side left -char ([char]::ConvertFromUtf32(0xE0B0))
Set-PromptSeparator -side right -char ([char]::ConvertFromUtf32(0xE0B2))
Add-Datasource path
Add-Datasource history
Add-Datasource project
Add-Datasource git
Add-Datasource kubernetes
Set-Prompt -index 0  -line 1 -side left -ForegroundColor darkYellow -BackgroundColor darkRed -Content {
    if ($prompt['project-root']) {
        "{0} " -f $prompt['project-root']
    } else {
        $prompt['path-current']
    }
}
Set-Prompt -index 1 -line 1 -side left -FG Blue -bg yellow -Content {
    " {0} " -f $prompt['git-commit']
}
Set-Prompt -index 2 -line 1 -side left -fg yellow -bg cyan -content {
    if ($prompt['project-root']) {
        " {0} " -f $prompt['project-projectPath']
    }
}
Set-Prompt -index 3 -line 1 -side left -fg darkred -bg green -content {
    " {0} " -f $prompt['kubernetes-Cluster']
}
Set-Prompt -line 1 -side right -fg yellow -bg cyan -content {
    " {0} " -f $prompt['history-Id']
}
Set-Prompt -line 1 -side right -fg darkred -bg green -content {
    " {0:mm}:{0:ss} " -f $prompt['history-Duration']
}
Set-Prompt -line 2 -side left -NoSeparator -content  {
    " PS >"
}
Set-PSReadLineOption -ExtraPromptLineCount 1
