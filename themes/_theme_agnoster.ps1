Set-PromptSeparator -side left -char ([char]::ConvertFromUtf32(0xE0B0))
Add-Datasource path
Add-Datasource history
Add-Datasource host
#Add-Datasource git
Add-Datasource kubernetes
#Add-Datasource Project

Set-PSReadLineOption -ExtraPromptLineCount 0
# Initial Separator
Set-Prompt -index 0 -side left -line 1 -bg Black -Content { [char]0 }
# Root indicator
# Set-Prompt -index 0 -side left -line 1 -fg "#100e23" -bg "#ffe9aa" -Content { " " }

# Session
Set-Prompt -index 1 -side left -line 1 -fg "#100e23" -bg "#ffffff" -Content {
    " {0}@{1} " -f $prompt['host-USER'], $prompt['host-COMPUTER']
}

# Path
Set-Prompt -index 2 -side left -line 1 -fg "#100e23" -bg "#91ddff" -Content {
    " {0} " -f $prompt['path-current']
}

# Gitahead
Set-Prompt -index 3 -side left -line 1 -fg "#193549" -bg "#95ffa4" -Content {
    " [ Git ] "
}

# Exit Code
Set-Prompt -index 4 -side left -line 1 -fg "#FFFFFF" -bg "#FF8080" -Content {
    if ($prompt['history-lastExit'] -eq $false) {
        " {0} " -f $prompt['history-error']
    }
}
