Add-Datasource path
Add-Datasource history
Add-Datasource host
Add-Datasource git
Add-Datasource kubernetes
#Add-Datasource Project

Set-PSReadLineOption -ExtraPromptLineCount 1

# Session
$indicator = "#CB4B16"
$esc = [char]27
$white = "#FFFFFF"

Set-Prompt -index 0 -side left -line 1 -fg $indicator -Content {
    "{0}[" -f ([char]::ConvertFromUtf32(0x250f))
}
Set-Prompt -index 1 -side left -line 1 -fg "#FFFFFF" -Content {
    $prompt['host-USER']
}
Set-Prompt -index 2 -side left -line 1 -fg $indicator -Content {
    "]"
}

Set-Prompt -index 3 -side left -line 1 -fg "#FFFFFF" -Content {
    $indicator = "#CB4B16"
    $esc = [char]27
    $white = "#FFFFFF"
    if ($prompt['git-gitDir']) {
        @(
            ConvertTo-ColoredString -foregroundColor $indicator -text "[" -textOnly
            ConvertTo-ColoredString -foregroundColor $white -text (Get-GitPrompt) -textOnly
            ConvertTo-ColoredString -ForegroundColor $indicator -text ']' -textonly
            "${esc}[0m"
        ) -join ''
    }
}

Set-Prompt -index 4 -side left -line 1 -fg "#FFFFFF" -Content {
    $indicator = "#CB4B16"
    $esc = [char]27
    $white = "#FFFFFF"
    if ($prompt['history-lastExit'] -eq $false) {
        @(
            ConvertTo-ColoredString -foregroundColor $indicator -text "[" -textOnly
            ConvertTo-ColoredString -foregroundColor $white -text "x$($prompt['history-error'])" -textOnly
            ConvertTo-ColoredString -ForegroundColor $indicator -text ']' -textonly
            "${esc}[0m"
        ) -join ''
    }
}


Set-Prompt -index 0 -side left -line 2 -fg $indicator -Content {
    "$([char]::ConvertFromUtf32(0x2516))["
}
Set-Prompt -index 1 -side left -line 2 -fg "#FFFFFF" -Content {
    $prompt['path-current']
}
Set-Prompt -index 2 -side left -line 2 -fg $indicator -Content {
    "]>"
}
