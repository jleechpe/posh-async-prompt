$sourceCompleter = {
    param(
        $commandName,
        $parameterName,
        $wordToComplete,
        $commandAst,
        $fakeBoundParameter
    )

    $themeNames | Where-Object {
        $f = "^{0}.*" -f $wordToComplete
        $_ -match $f
    } | ForEach-Object {
        New-Object System.Management.Automation.CompletionResult(
            $_,
            $_,
            'ParameterValue',
            $_
        )
    }
}

Get-Command *-PromptTheme | ForEach-Object {
    $splat = @{
        Command     = $_
        Parameter   = "Theme"
        ScriptBlock = $sourceCompleter
    }
    Register-ArgumentCompleter @splat
}

