$sourceCompleter = {
    param(
        $commandName,
        $parameterName,
        $wordToComplete,
        $commandAst,
        $fakeBoundParameter
    )

    if ($commandName -eq 'Get-DataSource') {
        $names = Get-DataSource -active
    } else {
        $names = Get-DataSource -list
    }
    $names | Where-Object {
        $f = "^{0}.*" -f $wordToComplete
        $_ -match $f
    } | ForEach-Object {
        New-Object System.Management.Automation.CompletionResult(
            $_,
            $_,
            'ParameterValue',
            $_
        )
    }
}

Get-Command *-DataSource | ForEach-Object {
    $splat = @{
        Command     = $_
        Parameter   = "Name"
        ScriptBlock = $sourceCompleter
    }
    Register-ArgumentCompleter @splat
}

Get-Command *-PromptVariables | ForEach-Object {
    $splat = @{
        Command     = $_
        Parameter   = "Source"
        ScriptBlock = $sourceCompleter
    }
    Register-ArgumentCompleter @splat
}
