$script:moduleName = Split-Path -leaf $PSScriptRoot
$script:moduleDir = $PSScriptRoot

$folders = @(
    'classes'
    'functions'
    'datasources'
    'completers' # Completers must come after functions to work
)

$folders | ForEach-Object {
    Get-ChildItem (Join-Path $PSScriptRoot $_) *.ps1
} | ForEach-Object {
    . $_.fullname
}

$script:dataDir = Join-Path $PSScriptRoot "datasources"
$script:themeDir = Join-Path $PSScriptRoot "themes"
$script:themeNames = Get-ChildItem -path $themeDir -Filter _theme_*.ps1 | ForEach-Object {
    $_.basename -replace "_theme_"
}
$script:DSNames = Get-ChildItem -Path $dataDir -Filter _data_*.ps1 | ForEach-Object {
    $_.basename -replace "_data_"
}

# Sync object for prompt variables
$prompt = [Hashtable]::Synchronized(@{})
$asyncPromptState = [Hashtable]::Synchronized(@{})
$dataSources = [Hashtable]::Synchronized(@{})

$promptSegments = @{}

function Prompt {
    $hist_exit = $?
    $hist_error = $global:error[0]
    if (-not $asyncPromptState.renderEngine) {
        $asyncPromptState.renderEngine = Register-EngineEvent -SourceIdentifier PowerShell.OnIdle -Action {
            if ($asyncPromptState.renderState -eq 'rerender') {
                $asyncPromptState.isIdleRender = 'yes'
                Refresh-PromptVariables -source project
                [Microsoft.Powershell.PSConsoleReadline]::InvokePrompt()
                $asyncPromptState.isIdleRender = 'no'
            }
        }
    }
    # Grab exit status and use to update prompt status
    $hist_id = (Get-History -count 1).id
    if ($prompt['history-Id'] -ne $hist_id) {
        Get-UpdatedPromptVariables $hist_id $hist_exit $hist_error
    }

    # Update Project Last
    Refresh-PromptVariables -source project
    # Ensure promptString is correct

    $asyncPromptState.renderState = "done"
    # Return appropriate string
    Render-Prompt $promptSegments
    return " "
}

$OnRemove = {
    Get-DataSource -Active | ForEach-Object {
        $source = $datasources[$_]
        # Remove all jobs/subscribers
        if ($source.subscriber) {
            $source.subscriber | Remove-Job -force
        }
        if (
            $source.job -and
            $source.job.getType().FullName -eq 'System.Management.Automation.PSRemotingJob'
        ) {
            $source.job | ForEach-Object {
                Remove-Job -name $_.name -ErrorAction Ignore
            }
        }
    }
    $asyncPromptState.renderEngine | Remove-Job -force -ErrorAction Ignore
}

$ExecutionContext.SessionState.Module.OnRemove += $OnRemove

$functions = @(
    '*-DataSource',
    'prompt',
    'Get-ParentItem',
    '*-PromptTheme',
    'Set-Prompt*',
    'ConvertTo-ColoredString',
    '*-PromptVariables'
)
Export-ModuleMember -Function $functions
Export-ModuleMember -Variable asyncpromptstate, prompt
