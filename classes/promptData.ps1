enum promptDataAsync {
    false    = 0
    true     = 1
    optional = -1
}

enum promptDataRefresh {
    false = 0
    prompt = 1
    async = -1
}

enum promptAsyncMethod {
    FileWatch
    Timer
    Job
    Synchronous
}

class promptData {
    [string]$name
    [promptDataAsync]$async = 0
    [promptDataRefresh]$refresh = 0
    [scriptblock]$outputs
    [promptAsyncMethod]$AsyncMethod = "Synchronous"
    $AsyncFile
    $AsyncInterval

    hidden $subscriber
    hidden $job
    hidden [string]$projectRoot

    promptData([object]$data) {
        $this.name        = $data.name
        $this.async       = $data.async
        $this.refresh     = $data.refresh
        $this.outputs     = $data.outputs
        $this.projectRoot = $data.projectRoot
        # Logic for Async triggers as datasource
        if ($this.async -ne 0) {
            $this.AsyncMethod = [promptAsyncMethod]$data.AsyncMethod

            switch ([int]$this.AsyncMethod) {
                0 {
                    $this.AsyncFile = Resolve-Path $data.AsyncFile
                    break
                }
                1 {
                    $this.AsyncInterval = $data.AsyncInterval
                    break
                }
                2 {
                    break
                }
                default {
                    throw "Invalid Async Method"
                    break
                }
            }
        }
    }
}
