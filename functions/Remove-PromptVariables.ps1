function Remove-PromptVariables {
    [CMDLetBinding(
         SupportsShouldProcess,
         ConfirmImpact = "Low"
     )]
    param(
        [String]$source
    )

    if ($PSCmdlet.ShouldProcess()) {
        $prompt.keys | Where-Object {
            $_ -cmatch $source
        } | ForEach-Object {
            $prompt.TryRemove($_,[ref]$null) | Out-Null
        }
    }
}
