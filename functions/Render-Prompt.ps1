function Render-Prompt {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingWriteHost', "")]
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseApprovedVerbs', "")]
    param(
        $promptSegments
    )
    $escRegex = "$([char]27)\[.*?m"
    $lines = $promptSegments.keys | Where-Object {
        $_ -match "Line.*"
    } | Sort-Object
    $lines | ForEach-Object {
        $rightLength = 0
        $leftSegments = $promptSegments[$_].left
        $leftCount = $leftSegments.count
        $leftOldSep = $true
        $segment = $null
        for ($left = 0; $left -lt $leftCount; $left++) {
            $item = $leftSegments[$left]
            $item.text = &$item.Content
            if ($left -ne 0 -and -not $leftOldSep) {
                $segment = @{
                    text = &{$promptSegments['LeftSeparator'].Char}
                    ForegroundColor = $oldbg
                    BackgroundColor = $item.BackgroundColor
                }
            }
            if ($item.text -notmatch '^\s*$') {
                $result = _renderSegment -segment $segment
                $result = _renderSegment -segment $item
                $oldbg = $result.bg
                $leftOldSep = $item.NoSeparator
            }
        }
        if (!$leftOldSep) {
            $segment = @{
                text = $promptSegments['LeftSeparator'].Char
                ForegroundColor = $oldbg
                BackgroundColor = $null
            }
            $result = _renderSegment -segment $segment
        }
        if ($promptSegments[$_].right) {
            $buffer = $host.UI.RawUI.BufferSize.Width
            $rightSegments = $promptSegments[$_].right
            $rightCount = $rightSegments.count
            # Get Right length for padding
            $sep = $promptSegments['RightSeparator'].Char.length
            for ($right = 0; $right -lt $rightCount; $right++) {
                $segment = $rightSegments[$right]
                $text = ((&$segment.Content) -replace $escRegex).length
                $sep = if (-not $segment.NoSeparator -and $promptSegments['RightSeparator']) {
                    $sep
                } else { 0 }
                $rightLength = $rightLength + $text + $sep
            }
            $leftLoc = $host.ui.rawui.cursorposition -replace ',.*'
            $pad = $buffer - $leftLoc - $rightLength
            Write-Host -noNewline ("{0,$pad}" -f '')
            for ($right = 0; $right -lt $rightCount; $right++) {
                if (-not $rightSegments[$right].NoSeparator) {
                    $segment = @{
                        text = $promptSegments['RightSeparator'].Char
                    }
                    if ($right -eq 0 ) {
                        $segment.BackgroundColor = $null
                    }
                    else {
                        $segment.BackgroundColor = $oldbg
                    }
                    $segment.ForegroundColor = $rightSegments[$right].BackgroundColor
                    $result = _renderSegment -segment $segment
                }
                $item = $rightSegments[$right]
                $item.text = &$item.Content
                $result = _renderSegment -segment $item
                $oldbg = $result.bg
            }
        }
        if ($lines.count -gt 1 -and $_ -ne $lines[-1]) {
            Write-Host ''
        }
    }
}

function _renderSegment {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingWriteHost', "")]
    param(
        $segment
    )
    $escRegex = "$([char]27)\[.*?m"
    # Set Background and Foreground
    if ($segment.ForegroundColor) {
        $fg = $segment.ForegroundColor
    }
    if ($segment.BackgroundColor) {
        $bg = $segment.BackgroundColor
    }
    if ($segment.Invert) {
        $fg, $bg = $bg, $fg
    }
    $splat = @{
        text = $segment.text
    }
    if ($fg -and (!$segment.resetAll -or $segment.resetFG)) {
        $splat.ForegroundColor = $fg
    }
    if ($bg -and (!$segment.resetAll -or $segment.resetBG)) {
        $splat.BackgroundColor = $bg
    }
    ConvertTo-ColoredString @splat | ForEach-Object {
        Write-Host @_ -NoNewLine
    }
    $return = @{
        length = $segment.text.length -replace $escRegex
    }
    if ($fg) {
        $return.fg = $fg
    }
    if ($bg) {
        $return.bg = $bg
    }
    return $return
}
