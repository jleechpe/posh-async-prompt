function Get-ParentItem {
    [CMDLetBinding()]
    param(
        $path = $pwd,
        [string]$filter
    )

    # Leverage -force to ensure hidden files are found
    $item = Get-ChildItem -filter $filter -Path $path -Force

    if ($item) {
        return $item
    } else {
        if (Split-Path $path) {
            Get-ParentItem -Path (Split-Path $path) -filter $filter
        } else {
            return $null
        }
    }
}
