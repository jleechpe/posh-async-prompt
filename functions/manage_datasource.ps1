function Add-DataSource {
    [CMDLetBinding()]
    param(
        [Parameter(ValueFromPipeline)]
        [string]$Name
    )

    PROCESS {
        $dataSources["$Name"] = &"_load_${Name}"

        Refresh-PromptVariables -Source $Name -force
    }
}

function Remove-DataSource {
    [CMDLetBinding(
         SupportsShouldProcess,
         ConfirmImpact = "Low"
     )]
    param($Name)

    if ($PSCmdlet.ShouldProcess()) {
        $dataSources.TryRemove($Name,[ref]$null) | Out-Null

        Remove-PromptVariables -Source $Name
    }
}

function Get-DataSource {
    [CMDLetBinding()]
    param(
        [Parameter(ParameterSetName = 'list',
                   Mandatory = $true
                  )]
        [switch]$list,
        [Parameter(ParameterSetName = 'active',
                   Mandatory = $true
                  )]
        [switch]$active,
        [Parameter(ParameterSetName = 'single',
                   Mandatory = $true
                  )]
        [string]$Name
    )

    switch ($PSCmdlet.ParameterSetName) {
        'list'   { $DSNames }
        'active' { $dataSources.keys }
        'single' { $dataSources[$name] }
    }
}
