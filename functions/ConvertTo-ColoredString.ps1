function ConvertTo-ColoredString {
    param(
        $foregroundColor,
        $backgroundColor,
        $text,
        [switch]$TextOnly
    )



    $esc = [char]27
    $endString = "$esc[0m"
    $out = @{}
    if ($ForegroundColor) {
        $fgIsName = $foregroundColor -in [Enum]::GetValues([System.ConsoleColor])
        if ($fgIsName -and $ForegroundColor) {
            $out.ForegroundColor = $foregroundColor
        }
        elseif ($foregroundcolor -match "#[0-9a-f]{6}") {
            $fgString = "$esc[38;2;{0}m" -f (_hex_to_rgb $foregroundColor)
            $text = "{0}{1}" -f $fgString, $text
        }
    }
    if ($BackgroundColor) {
        $bgIsName = $backgroundColor -in [Enum]::GetValues([System.ConsoleColor])
        if ($bgIsName) {
            $out.BackgroundColor = $backgroundColor
        }
        elseif ($backgroundColor  -match "#[0-9a-f]{6}") {
            $bgString = "$esc[48;2;{0}m" -f (_hex_to_rgb $backgroundColor)
            $text = "{0}{1}" -f $bgString, $text
        }
    }
    if (!$fgIsName -or !$bgIsName) {
        $text = "{0}{1}" -f $text, $endString
    }
    $out.object = $text
    if ($TextOnly) {
        $text
    } else {
        $out
    }
}

# Convert hex RGB string (#FFFFFF) to integer ASCII escape codes
# (255;255;255)
function _hex_to_rgb {
    param($hex)

    $vals = $hex -replace '#' -replace '(..)(?!$)', '$1;' -split ';' | ForEach-Object {
        [int]"0x${_}"
    }
    $vals -join ";"
}

# Validate that the color passed through is either a console color or
# a valid 6 character hex string.

function Validate-Color {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseApprovedVerbs', "")]
    param($color)

    (($color -in [Enum]::GetValues([System.ConsoleColor])) -or
        $color -match "\A#[0-9a-f]{6}\z")
}
