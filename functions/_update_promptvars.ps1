# Update specific underlying variables and trigger variable updates.
function Get-UpdatedPromptVariables {
    param (
        $hist_id,
        $hist_exit,
        $hist_error
    )
    $prompt['history-Id'] = $hist_id
    $prompt['history-lastExit'] = $hist_exit
    if ($hist_exit -eq $true) {
        $prompt['history-error'] = $null
    } else {
        $errorString = $hist_error.FullyQualifiedErrorId -replace 'exception'
        $errorString = $errorString -replace ',.*'
        $prompt['history-error'] = $errorString
    }
    foreach ($source in $datasources.keys) {
        Refresh-PromptVariables -Source $source
    }
}
