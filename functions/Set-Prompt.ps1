function Set-Prompt {
    [CMDLetBinding(DefaultParameterSetName = 'colors')]
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseShouldProcessForStateChangingFunctions', "")]
    param(
        [ValidateSet('left', 'right')]
        [string]$side = 'left',
        [int]$line,
        [Parameter(
            ParameterSetName = 'colors'
        )]
        [Parameter(
            ParameterSetName = 'resetBG'
        )]
        [Alias('fg')]
        [ValidateScript({Validate-Color $_})]
        $ForegroundColor,
        [Parameter(
            ParameterSetName = 'resetFG'
        )]
        [Parameter(
            ParameterSetName = 'colors'
        )]
        [Alias('bg')]
        [ValidateScript({Validate-Color $_})]
        $BackgroundColor,
        [Parameter(
            ParameterSetName = 'resetFG',
            Mandatory = $true
        )]
        [switch]$ResetFG,
        [Parameter(
            ParameterSetName = 'resetBG',
            Mandatory = $true
        )]
        [switch]$ResetBG,
        [Parameter(
            ParameterSetName = 'resetAll',
            Mandatory = $true
        )]
        [switch]$ResetAll,
        [scriptBlock]$content,
        [Parameter(ParameterSetName = 'colors')]
        [Parameter(ParameterSetName = 'resetFG')]
        [Parameter(ParameterSetName = 'resetBG')]
        [switch]$invert,
        [int]$index,
        [switch]$replace,
        [switch]$NoSeparator
    )

    $segment = @{
        Content = $content
    }

    switch ($PSCmdlet.ParameterSetName) {
        "resetAll" {
            $segment.Add('resetAll',$true)
            break
        }
        "resetFG" {
            $segment.Add('resetFG',$true)
            $segment.Add('BackgroundColor',$BackgroundColor)
            break
        }
        "resetBG" {
            $segment.Add('resetBG',$true)
            $segment.Add('ForegroundColor',$ForegroundColor)
            break
        }
        "colors" {
            $segment.Add('ForegroundColor',$ForegroundColor)
            $segment.Add('BackgroundColor',$BackgroundColor)
            break
        }
    }
    if ($invert) {
        $segment.Add('Invert',$invert)
    }
    if ($NoSeparator) {
        $segment.Add('NoSeparator', $NoSeparator)
    }

    $lineName = "Line${line}"
    if (!$promptSegments[$lineName]) {
        $promptSegments[$lineName] = @{
            Left = [System.Collections.ArrayList]@()
            Right = [System.Collections.ArrayList]@()
        }
    }
    if ($replace) {
        $promptSegments[$lineName][$side].Item($index) = $segment
    } else {
        $promptSegments[$lineName][$side].insert($index, $segment)
    }
}

