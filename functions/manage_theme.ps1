function Get-PromptTheme {
    param(
        $Theme,
        [switch]$list
    )

    if ($list) {
        return $script:themeNames
    } else {
        return $asyncPromptState.theme
    }
}

function Use-PromptTheme {
    param(
        $Theme
    )

    $promptSegments.clear()
    $file = Join-Path $themeDir "_theme_${theme}.ps1"
    . $file
    $asyncPromptState.theme = $theme
}
