function Set-PromptSeparator {
    [CMDLetBinding()]
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseShouldProcessForStateChangingFunctions', "")]
    param(
        [ValidateSet('left', 'right')]
        $side,
        $char
    )

    $promptSegments["${side}Separator"] = @{
        Char = $char
    }
}
