function Get-GitPrompt {
    [CmdletBinding()]
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseDeclaredVarsMoreThanAssignments', "")]
    param()
    $branch           = [char]::ConvertFromUtf32(0xE0A0)
    $branch_identical = [char]::ConvertFromUtf32(0xF0C9)
    $branch_ahead     = [char]::ConvertFromUtf32(0xF176)
    $branch_behind    = [char]::ConvertFromUtf32(0xF175)
    $branch_gone      = [char]::ConvertFromUtf32(0x2262)

    $local_working = [char]::ConvertFromUtf32(0xF044)
    $local_index   = [char]::ConvertFromUtf32(0xF046)
    $stash_count   = [char]::ConvertFromUtf32(0xF692)

    $commit     = [char]::ConvertFromUtf32(0xF417)
    $tag        = [char]::ConvertFromUtf32(0XF412)
    $rebase     = [char]::ConvertFromUtf32(0xE728)
    $cherryPick = [char]::ConvertFromUtf32(0xE29B)
    $merge      = [char]::ConvertFromUtf32(0xE727)
    $noCommits  = [char]::ConvertFromUtf32(0xF594)

    $upstream_github      = [char]::ConvertFromUtf32(0xF408)
    $upstream_gitlab      = [char]::ConvertFromUtf32(0xF296)
    $upstream_bitbucket  = [char]::ConvertFromUtf32(0xF171)
    $upstream_Git         = [char]::ConvertFromUtf32(0xE5FB)

    $str = [System.Collections.ArrayList]@()

    # Upstream and Branch State
    if ($prompt['git-upstreamHost'] -notmatch '^\S+') {
        $upstreamName = 'upstream_git'
    } else {
        $upstreamName = 'upstream_{0}' -f $prompt['git-upstreamHost']
    }
    $str.Add("{0}" -f (Get-Variable $upstreamName -ValueOnly)
            ) | out-null
    $null = $str.add(' ')
    $null = $str.add($branch)
    $null = $str.add($prompt['git-branch'])
    if ($prompt['git-upstreamGone']) {
        $str.add($branch_gone) | Out-Null
    }
    $str.add(' ') | Out-Null
    $ahead = if ($prompt['git-aheadBy']) {
        '{0}{1}' -f $branch_ahead, $prompt['git-aheadBy']
    }
    $behind = if ($prompt['git-behindBy']) {
        '{0}{1}' -f $branch_behind, $prompt['git-behindBy']
    }
    if ($ahead -or $behind) {
        $branchState = "{0}{1}" -f $ahead,$behind
    } else {
        $branchState = $branch_identical
    }
    $null = $str.add($branchState)

    # Working State
    $work_Add = if ($prompt['git-working'].added) {
        "+$($prompt['git-working'].added.count)"
    }
    $work_Mod = if ($prompt['git-working'].modified) {
        "~$($prompt['git-working'].modified.count)"
    }
    $work_Del = if ($prompt['git-working'].deleted) {
        "-$($prompt['git-working'].deleted.count)"
    }
    $work_Utr = if ($prompt['git-working'].untracked) {
        "?$($prompt['git-working'].untracked.count)"
    }
    if ($work_add -or $work_mod -or $work_del -or $work_utr) {
        $work = "{0}{1}{2}{3}{4}" -f $local_working,$work_Add,$work_Mod,$work_Del,$work_Utr
    }

    # Index State
    $index_Add = if ($prompt['git-index'].added) {
        "+$($prompt['git-index'].added.count)"
    }
    $index_Mod = if ($prompt['git-index'].modified) {
        "~$($prompt['git-index'].modified.count)"
    }
    $index_Del = if ($prompt['git-index'].deleted) {
        "-$($prompt['git-index'].deleted.count)"
    }
    $index_Utr = if ($prompt['git-index'].untracked) {
        "?$($prompt['git-index'].untracked.count)"
    }
    if ($index_add -or $index_mod -or $index_del -or $index_utr) {
        $index = "{0}{1}{2}{3}{4}" -f $local_index,$index_Add,$index_Mod,$index_Del,$index_Utr
    }

    if ($work -and $index) {
        $state = " {0} | {1} " -f $work, $index
    } elseif ($work) {
        $state = " {0} " -f $work
    } elseif ($index) {
        $state = " {0} " -f $index
    }
    $null = $str.add($state)

    Write-Verbose $($str -join '')
    $str -join ''
}

function Edit-HomeDirSegment {
    param(
        [Parameter(ValueFromPipeline)]
        [string]$path,
        [switch]$useIcon
    )

    if ($useIcon) {
        $replace = [char]::ConvertFromUtf32(0xF7DD)
    } else {
        $replace = '~'
    }
    $path -replace ((Resolve-Path "~") -replace '\\','\\'),$replace
}

function Get-ProjectPath {
    [CMDLetBinding()]
    param(
        [ValidateScript({Validate-Color $_})]
        [string]$projectFG,
        [ValidateScript({Validate-Color $_})]
        [string]$projectBG,
        [switch]$includeGit,
        [ValidateScript({Validate-Color $_})]
        $subPathFG,
        [ValidateScript({Validate-Color $_})]
        $subPathBG
    )

    DynamicParam {
        if ($includeGit) {
            $dict = [System.Management.Automation.RuntimeDefinedParameterDictionary]::New()
            $attr = [System.Management.Automation.ParameterAttribute]::New()
            $coll = [System.Collections.ObjectModel.Collection[System.Attribute]]::New()
            $valid = [System.Management.Automation.ValidateScriptAttribute]::New({Validate-Color $_})
            $coll.Add($attr)
            $coll.Add($valid)

            $FG = [System.Management.Automation.RuntimeDefinedParameter]::New('gitFG',[string],$coll)
            $BG = [System.Management.Automation.RuntimeDefinedParameter]::New('gitBG',[string],$coll)

            $dict.Add('gitFG',$fg)
            $dict.Add('gitBG',$bg)
            $dict
        }
    }

    PROCESS {
        if ((Get-Datasource -Name Project) -and $prompt['project-root']) {
            $projectPath = $prompt['project-root'] | Edit-HomeDirSegment -useIcon
            $projectSubPath = $prompt['project-projectPath']

            $projSplat = @{
                ForegroundColor = $projectFG
                BackgroundColor = $projectBG
                text            = $projectPath
            }
            $projectPath = ConvertTo-ColoredString @projSplat -textonly
            $subSplat = @{
                ForegroundColor = $subPathFG
                BackgroundColor = $subPathBG
                text            = $projectSubPath
            }
            $projectSubPath = ConvertTo-ColoredString @subSplat -textonly
            if ($includeGit -and $prompt['git-gitDir']) {
                $gitData = "\[{0}]\" -f (Get-GitPrompt)
                $gitSplat = @{
                    ForegroundColor = $gitFG
                    BackgroundColor = $gitBG
                    text            = $gitData
                }
                $gitData = ConvertTo-ColoredString @gitSplat -textonly
            }
            else {
                $gitData = "\"
            }

            return " {0}{1}{2}" -f $projectPath, $gitData, $projectSubPath
        }

        else {
            $projSplat = @{
                ForegroundColor = $projectFG
                BackgroundColor = $projectBG
                text            = $prompt['path-current'].path
            }
            $path = ConvertTo-ColoredString @projSplat -textOnly
            "{0}" -f $path
        }
    }
}
