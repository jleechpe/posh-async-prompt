function Refresh-PromptVariables {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSUseApprovedVerbs', "")]
    [CMDLetBinding()]
    param(
        [switch]$force,
        [string]$source
    )

    $data = $datasources[$source]

    $filter = @{
        Force  = $force
    }

    switch ([int]$data.async) {
        # No
        0 {
            $filter.'Immediate' = $true
        }
        # Yes
        1 {
            $filter.'Async' = $true
        }
        # Optional
        -1 {
            $filter.'ByRefresh' = $true
        }
        default {
            Write-Warning "Undefined async"
        }
    }

    switch ([int]$data.refresh) {
        # No
        0 {
            $filter.'Skip' = $true
        }
        # Yes
        1 {
            $filter.'Refresh' = $true
        }
        # Async
        -1 {
            $filter.'Async' = $true
        }
        default {
            Write-Warning "Undefined Refresh"
        }
    }

    # Return if skipped and not forced
    if ($filter.skip -and -not $filter.force) {
        return
    }

    # Synchronous Refresh if specified
    if (
        $filter.immediate -or
        ($filter.ByRefresh -and $filter.Refresh)
    ) {
        $values = &$datasources[$source].outputs
        Update-PromptVariables -Source $source -Values $values

    }
    elseif ($filter.Async) {
        _doRefreshAsync $source
    }
}

# Perform actual variable update based on data pulled via DataSources.
function Update-PromptVariables {
    param(
        $source,
        $values
    )
    foreach ($key in $values.keys) {
        $variable = "{0}-{1}" -f $source, $key
        $prompt[$variable] = $values[$key]
    }
}

# Trigger appropriate Async method
function _doRefreshAsync {
    param(
        $source
    )

    $data = $datasources[$source]
    $method = $data.AsyncMethod
    $name = $data.name
    [scriptblock]$task = $data.outputs
    switch ($method) {
        "Job" {
            # Register Subscriber
            if (-not $data.Subscriber) {
                $id = "Prompt.$name"
                $data.Subscriber = Register-EngineEvent -SourceIdentifier $id -Action {
                    Update-PromptVariables -Source $args.name -Values $args.output
                    $asyncPromptState.renderState = 'rerender'
                }
            }
            if ($data.job) {
                Get-Job -name $name | Where-Object {
                    $_.id -lt $data.job.id
                } | Stop-Job -PassThru | Remove-Job
            }
            $job = {
                $name = $args[0]
                set-location $args[2]
                Register-EngineEvent -Forward -SourceIdentifier "Prompt.$name"
                $task = [scriptblock]::Create($args[1])
                $out = &$task
                New-Event -SourceIdentifier "Prompt.$name" -EventArguments @{
                    name = $name
                    output = $out
                }
            }
            $data.job = Start-Job -Name $name -ScriptBlock $job -ArgumentList $name,$task,$pwd
        }
        "FileWatch" {
            $action = {
                $name = $event.MessageData.name
                $task = $event.MessageData.task
                $out = &$task
                Update-PromptVariables -Source $name -Values $out
            }

            $path = Split-Path -parent $data.AsyncFile
            $file = Split-Path -leaf $data.AsyncFile

            $AttrFilter = [IO.NotifyFilters]::LastWrite
            $data.job = New-Object System.IO.FileSystemWatcher -Property @{
                Path = $path
                Filter = $file
                NotifyFilter = $AttrFilter
                EnableRaisingEvents = $true
            }

            # Register Subscriber
            if (-not $data.Subscriber) {
                $out = &$task
                Update-PromptVariables -Source $data.name -Values $out
                $id = "Prompt.$name"
                $splat = @{
                    InputObject = $data.job
                    EventName = "Changed"
                    Action = $action
                    MessageData = @{
                        name = $name
                        task = $task
                    }
                }
                $data.Subscriber = Register-ObjectEvent @splat
            }
        }
        "Timer" {
            $action = {
                $name = $event.MessageData.name
                $task = $event.MessageData.task
                $data = $event.MessageData.Data
                $out = &$task -Data $data
                if ($out) {
                    Update-PromptVariables -Source $name -Values $out
                    $asyncPromptState.renderState = 'rerender'
                }
            }
            if (-not $data.job) {
                $data.job = New-Object timers.timer
                $data.job.interval = 1000
                $data.job.enabled = $true
            }
            if (-not $data.Subscriber) {
                $out = &$task -Data $data
                Update-PromptVariables -Source $data.name -Values $out
                $id = "Prompt.$name"
                $splat = @{
                    InputObject = $data.job
                    EventName = "Elapsed"
                    SourceIdentifier = "timer.output"
                    Action = $action
                    MessageData = @{
                        name = $name
                        task = $task
                        Data = $data
                    }
                }
                $data.Subscriber = Register-ObjectEvent @splat
            }
        }
    }
}
